use std::cmp::Ordering;

pub struct Ia {
    // Put your fields here
}

impl Default for Ia {
    fn default() -> Self {
        // Init your IA here
        todo!()
    }
}

impl Ia {
    pub fn think(&mut self, message: Message) -> u32 {
        // Code your intelligence here
        todo!()
    }
}

pub enum Message {
    NewGame { max: u32 },
    Response(Ordering),
}

pub fn play(max: u32, ia: &mut Ia) -> u32 {
    let mut message = Message::NewGame { max };
    let mut score = 1;
    let secret = (rand::random::<u32>() % max) + 1;
    loop {
        let ia_choice = ia.think(message);
        message = match secret.cmp(&ia_choice) {
            Ordering::Less => Message::Response(Ordering::Less),
            Ordering::Equal => break score,
            Ordering::Greater => Message::Response(Ordering::Greater),
        };
        score += 1;
        if score > 1_000 {
            panic!("infinite loop");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let mut ia = Ia::default();
        for _ in 0..50 {
            assert!(play(100, &mut ia) < 8);
        }
        for _ in 0..500 {
            assert!(play(1000, &mut ia) < 11);
        }
        for _ in 0..5 {
            assert_eq!(play(1, &mut ia), 1);
        }
    }
}
